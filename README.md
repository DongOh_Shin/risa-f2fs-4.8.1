# README #
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution guidelines ###

* Commit as much as possible.
* Don't hesitate to make branches. 
    * For example, in the case of adding debugging messages, I can make a branch for this to check the errors. If I found the error, I can easily DELETE this by `git branch -D LOGGING_BRANCH`
    * `git cherry-pick` can pick the essential commits for debugging.
* master branch is just for maintaining the project. Make a pull request if you complete a piece of job. 



### What is this repository for? ###
* Build f2fs-alfs on linux kernel 4.8.1


### How do I get set up? ###

* Build mkfs and fsck in tools/ directory.
* Make a f2fs-alfs file system on a device. (i.g., `sudo mkfs.f2fs /dev/nvme0n1`)
* Check the sanity by fsck (i.g., `sudo fsck.f2fs /dev/nvme0n1`)
* Build file system kernel module and install it using `sudo insmod f2fs.ko`)
* Mount it.  (i.g., `sudo mount -t f2fs -o discard /dev/nvme0n1 /media/nvme0n1`)
* Feel free to request the other guidelines if you have any questions.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact